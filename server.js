const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const cors = require("cors");
const app = express();

const HTTP_PORT = process.env.PORT || 8080;
const MONGO_URI = process.env.MONGO_URI || "mongodb://localhost:27017/Marketplace";

// Connect to MongoDB
mongoose
  .connect(MONGO_URI)
  .then(() => console.log("Connected to MongoDB on " + MONGO_URI))
  .catch((err) => console.error("Could not connect to MongoDB", err));

// Define a schema and a model for products
const productSchema = new mongoose.Schema({
  name: String,
  description: String,
  price: Number,
  quantity: Number,
  category: String
});

const Product = mongoose.model("Product", productSchema);

// Add support for incoming JSON entities
app.use(bodyParser.json(), cors());

// Deliver the app's home page to browser clients
app.get("/", (req, res) => {
  res.json({ message: 'Welcome to DressStore application.' });
});

// Get all products
app.get("/api/products", async (req, res) => {
  try {
    if (req.query.name) {
      const searchQuery = req.query.name.replace(/\[|\]/g, '');
      const regex = new RegExp(searchQuery, 'i');

      const products = await Product.find({ name: regex });
      res.json(products);
    } else {
      const products = await Product.find().sort("name");
      res.json(products);
    }
  } catch (err) {
    res.status(500).send("Internal Server Error");
  }
});

// Get one product by ID
app.get("/api/products/:id", async (req, res) => {
  try {
    const product = await Product.findById(req.params.id);
    if (product) {
      res.json(product);
    } else {
      res.status(404).send("Resource not found");
    }
  } catch (err) {
    res.status(500).send("Internal Server Error");
  }
});

// Add new product
app.post("/api/products", async (req, res) => {
  try {
    const newProduct = new Product(req.body);
    await newProduct.save();
    res.status(201).json({
      message: `Added ${newProduct.name} as item identifier ${newProduct._id}`,
    });
  } catch (err) {
    console.error("Error adding new product:", err);

    // Check if the error is a validation error
    if (err.name === 'ValidationError') {
      res.status(400).json({ message: "Validation Error", errors: err.errors });
    } else {
      res.status(500).send("Internal Server Error");
    }
  }
});

// Edit existing product by ID
app.put("/api/products/:id", async (req, res) => {
  try {
    const updatedProduct = await Product.findByIdAndUpdate(
      req.params.id,
      req.body,
      { new: true }
    );
    if (updatedProduct) {
      res.json({
        message: `Updated item with identifier: ${req.params.id} to ${updatedProduct.name}`,
      });
    } else {
      res.status(404).send("Resource not found");
    }
  } catch (err) {
    res.status(500).send("Internal Server Error");
  }
});

// Delete product by ID
app.delete("/api/products/:id", async (req, res) => {
  try {
    const deletedProduct = await Product.findByIdAndDelete(req.params.id);
    if (deletedProduct) {
      res.status(200).json({ message: `Deleted product: ${deletedProduct.name}` });
    } else {
      res.status(404).send("Resource not found");
    }
  } catch (err) {
    res.status(500).send("Internal Server Error " + err);
  }
});

// Delete all products
app.delete("/api/products", async (req, res) => {
  try {
    await Product.deleteMany({});
    res.status(200).json({ message: "All products have been deleted." });
  } catch (err) {
    res.status(500).send("Internal Server Error");
  }
});

// Resource not found (this should be at the end)
app.use((req, res) => {
  res.status(404).send("Resource not found");
});

// Tell the app to start listening for requests
app.listen(HTTP_PORT, () => {
  console.log("Ready to handle requests on port " + HTTP_PORT);
});
