# DressStore Application

This is a Node.js application for managing products in the DressStore application.

## Prerequisites

- Node.js
- npm (Node Package Manager)
- MongoDB

## Installation

1. Clone the repository:

    ```sh
    git clone https://gitlab.com/manasv/marketplace-api.git
    cd marketplace-api
    ```

2. Install the dependencies:

    ```sh
    npm install
    ```

3. Start the application:

    ```sh
    node server
    ```

# The server will start and listen on port 8080 (or the port specified in the `.env` file).

## API Endpoints

### GET /

Returns a welcome message.

### GET /api/products

Returns a list of all products. Supports optional query parameter `name` for searching products by name.

### GET /api/products/:id

Returns a single product by its ID.

### POST /api/products

Adds a new product. The request body must be a JSON object with the following properties:
- `name` (string, required)
- `description` (string, required)
- `price` (number, required)
- `quantity` (number, required)
- `category` (string, required)

### PUT /api/products/:id

Updates an existing product by its ID. The request body must be a JSON object with the properties to update.

### DELETE /api/products/:id

Deletes a product by its ID.

### DELETE /api/products

Deletes all products.

## Example Requests

### Adding a New Product

```sh
curl -X POST http://localhost:8080/api/products \
-H "Content-Type: application/json" \
-d '{
  "name": "Elegant Silk Dress",
  "description": "A luxurious silk dress perfect for evening wear.",
  "price": 120.00,
  "quantity": 15,
  "category": "Dresses"
}'
